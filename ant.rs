use std::collections::HashSet;

fn digit_sum(coord: (i64, i64)) -> u64
{
    let nums: [u64; 2] = [coord.0.abs() as u64, coord.1.abs() as u64];
    let mut sum = 0;

    for x in nums.iter() {
        let mut n = *x;
        while n!=0 {
            sum+=n%10;
            n/=10;
        }
    }
    return sum;
}

fn step(coord: (i64, i64), cells: &mut HashSet<(i64, i64)>) {
    if digit_sum(coord) > 25  { return; }
    if cells.contains(&coord) { return; }
    cells.insert(coord);
    step((coord.0 + 1, coord.1), cells);
    step((coord.0 - 1, coord.1), cells);
    step((coord.0, coord.1 + 1), cells);
    step((coord.0, coord.1 - 1), cells);
}

fn main() {
    assert_eq!(digit_sum((1111, 1111)), 8);
    assert_eq!(digit_sum((0,0)), 0);
    assert_eq!(digit_sum((1234, 5678)), 36);
    assert_eq!(digit_sum((111, 222)), 9);

    let mut cells = HashSet::new();
    let start_coords = (1000, 1000);
    step(start_coords, &mut cells);

    println!("Visited cells:{}", cells.len());
    for x in cells.iter() {
        println!("X:{}, Y:{}", x.0, x.1)
    }

    println!("Visited cells count: {}", cells.len());
}